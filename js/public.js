
window.Vue = require('vue');
// require('vue-resource');

// window.Vuex = require('vuex').default;

window._ = require('lodash');
// window.Tether = require('tether');
window.$ = window.jQuery = require('jquery');

window.Waves = require('node-waves');

import Popper from 'popper.js';
window.Popper = Popper;

require('bootstrap');

import RangeSlider from 'vue-range-slider';
Vue.component('range-slider', RangeSlider);
Vue.component('start', require('./components/Start.vue'));
Vue.component('login', require('./components/Login.vue'));
Vue.component('investment-category', require('./components/InvestmentCategory.vue'));
Vue.component('explore-footer', require('./components/ExploreFooter.vue'));
Vue.component('nav-sidebar', require('./components/NavSidebar.vue'));
Vue.component('explorer', require('./components/Explorer.vue'));
Vue.component('explorer-card', require('./components/ExplorerCard.vue'));
Vue.component('individual-explorer', require('./components/IndividualExplorer.vue'));
Vue.component('profile', require('./components/Profile.vue'));
Vue.component('profile-settings', require('./components/ProfileSettings.vue'));