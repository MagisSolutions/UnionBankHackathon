var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var axios = require('axios');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var _ = require('lodash');
var fs = require('fs');
var moment = require('moment');
const queryString = require('query-string');

const CLIENT = require('./keys/client.json');
const USER = require('./keys/user.json');
const MERCHANT = require('./keys/merchant.json');

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true,
}));



///////////////
// Functions //
///////////////

function getBalance(accessToken) {
    return axios.get('https://api-uat.unionbankph.com/partners/sb/accounts/v1/balances', {
        headers: {
            accept: 'application/json',
            authorization: 'Bearer ' + accessToken,
            'x-ibm-client-id': CLIENT.id,
            'x-ibm-client-secret': CLIENT.secret,
        },
    });
}

////////////
// Socket //
////////////

io.on('connection', function (socket) {
    let authCode;
    let accessToken;

    socket.on('queryString', ({ url }) => {
        // obtain authCode
        authCode = queryString.parse(url.substring(url.indexOf('?') + 1));

        // obtain access token
        getAccessToken(authCode.code).then(response => {
            accessToken = response.data;
            return accessToken;
        })
            .then(accessToken => getBalance(accessToken.access_token))
            .then(balance => socket.emit('balance', balance.data))
            .catch(console.log);
    });

    socket.on('invest', ({ amount, particulars }) => {
        axios.post('https://api-uat.unionbankph.com/partners/sb/merchants/v1/payments/single', {
            senderPaymentId: Math.floor(Math.random() * 999999),
            paymentRequestDate: moment().toISOString(),
            amount: {
                value: amount,
                currency: 'PHP',
            },
            particulars,
        }, {
            headers: {
                accept: 'application/json',
                'x-ibm-client-id': CLIENT.id,
                'x-ibm-client-secret': CLIENT.secret,
                authorization: 'Bearer ' + accessToken.access_token,
                'content-type': 'application/json',
                'x-merchant-id': MERCHANT.id,
            },
        })
            .then(response => socket.emit('invested'))
            .then(() => getBalance(accessToken.access_token))
            .then(balance => socket.emit('balance', balance.data))
            .catch(error => console.log(error.response.data));
    });
});

////////////////////
// REST endpoints //
////////////////////

app.get('/', (req, res) => {
    res.redirect('https://api-uat.unionbankph.com/partners/sb/convergent/v1/oauth2/authorize'
        + '?client_id=' + CLIENT.id
        + '&response_type=code'
        + '&scope=payments balances'
        + '&redirect_uri=' + encodeURI('http://localhost:8000/auth/callback'));
    // res.sendFile(__dirname + '/index.html');
});

app.get('/auth/callback', (req, res) => {
    // after getting the auth code, get the access token
    res.sendFile(__dirname + '/index.html');
});

// app.get('/token', (req, res) => {
//     getAccessToken().then(response => console.log(response.data)).catch(() => {});
// });

app.get('/account/info', (req, res) => {
    axios.get('https://api-uat.unionbankph.com/partners/sb/accounts/109594480003', {
        headers: {
            accept: 'application/json',
            'x-ibm-client-id': CLIENT.id,
            'x-ibm-client-secret': CLIENT.secret,
        },
    }).then(response => res.send(response.data)).catch(console.log);
});

function getAccessToken(authCode) {
    return axios.post('https://api-uat.unionbankph.com/partners/sb/convergent/v1/oauth2/token', queryString.stringify({
        grant_type: 'authorization_code',
        client_id: CLIENT.id,
        code: authCode,
        redirect_uri: 'http://localhost:8000/auth/callback',
    }), {
        headers: {
            accept: 'text/html',
            'content-type': 'application/x-www-form-urlencoded',
        },
    });
}





////////////
// Listen //
////////////

http.listen(8000, () => {
    console.log('Hello! listening on *:8000');
});